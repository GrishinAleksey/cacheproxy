package com.grishin.utils;

import java.io.*;

public class SerializationService {
    public static void serialize(Object o, String fileName) {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(
                    new FileOutputStream(fileName));
            outputStream.writeObject(o);
        } catch (IOException e) {
            System.out.println("Объект не сериализован" + e);
        }
    }
    public static <T> T deserialize(String fileName) {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(
                    new FileInputStream(fileName));
            return (T) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Объект не десериализован" + e);
        }
        return null;
    }
}
