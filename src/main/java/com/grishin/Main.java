package com.grishin;

import com.grishin.proxy.CacheProxy;
import com.grishin.service.Service;
import com.grishin.service.impl.ServiceImpl;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        CacheProxy cacheProxy = new CacheProxy(new ServiceImpl(), "C:\\Users\\Алексей\\Documents\\cache");
        Service service = cacheProxy.cache(new ServiceImpl());
        service.doHardWork("1", 2, new Date());
        service.doWork("1");
        service.doHardWork("1", 3, new Date());
        service.doWork("1");

    }
}
