package com.grishin.proxy;

import com.grishin.proxy.annotations.Cache;
import com.grishin.utils.SerializationService;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class CacheProxy implements InvocationHandler {

    private final Object object;
    private final Map<String, Object> objectsInMemory;
    private static String cacheDir;

    public CacheProxy(Object object, String cacheDir) {
        this.object = object;
        this.objectsInMemory = new HashMap<>();
        CacheProxy.cacheDir = cacheDir;
    }

    public <T> T cache(Object object) {
        return (T) Proxy.newProxyInstance(
                object.getClass().getClassLoader(),
                object.getClass().getInterfaces(),
                new CacheProxy(object, cacheDir));
    }

    private Object key(Method method, Object[] args) {
        List<Object> key = new ArrayList<>();
        key.add(method);
        key.addAll(asList(args));
        return key;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (!method.isAnnotationPresent(Cache.class)) return invoke(method, args);
        CacheType cacheType = method.getAnnotation(Cache.class).cacheType();
        if (cacheType == CacheType.IN_MEMORY) {
            System.out.println("Объект записан в память");
            return memoryCache(method, args);
        } else {
            System.out.println("Объект записан в файл");
            return fileCache(method, args);
        }
    }

    private Object invoke(Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(object, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("В настоящий момент выполняющегося метода нет доступа к определению указанного класса, поля, метода или конструктора");
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

    private Object memoryCache(Method method, Object[] args) throws Throwable {
        if (!objectsInMemory.containsKey(key(method, args))) {
            Object result = invoke(method, args);
            if (method.getReturnType() == List.class) {
                int listSize = method.getAnnotation(Cache.class).listList();
                result = listCut(result, listSize);
            }
            objectsInMemory.put((String) key(method, args), result);
        }
        return objectsInMemory.get(key(method, args));
    }

    private Object fileCache(Method method, Object[] args) throws Throwable {
        String fileName = fileName(args, method);
        Object result;
        File file = new File(cacheDir + fileName);

        if (!file.exists()) {
            result = invoke(method, args);
            if (method.getReturnType() == List.class) {
                int listSize = method.getAnnotation(Cache.class).listList();
                result = listCut(result, listSize);
            }
            SerializationService.serialize(result, fileName);
            return result;
        } else {
            return SerializationService.deserialize(fileName);
        }
    }

    private ArrayList<?> listCut(Object o, int listSize) {
        List<?> list = (List) o;

        if (list.size() <= listSize) {
            return new ArrayList<>(list);
        }
        list = list.subList(0, listSize);
        return new ArrayList<>(list);
    }

    private String fileName(Object[] args, Method method) {
        return "Cache_" + method.getReturnType().getName() + "_" + args[0];
    }
}
